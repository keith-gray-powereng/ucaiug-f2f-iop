import logging
import sys
from datetime import datetime
from importlib.metadata import version
from logging.config import dictConfig
from pathlib import Path

import click
from lxml import etree
from yaml import CSafeLoader
from yaml import load

scl_namespaces = {
    "xs": "http://www.w3.org/2001/XMLSchema",
    "scl": "http://www.iec.ch/61850/2003/SCL",
}


def configure_logging(log_level):
    config_file = str(
        Path(getattr(sys, "_MEIPASS", Path(__file__).parent))
        / Path("resources")
        / Path("logging-config.yaml")
    )
    with open(config_file, "rt") as f:
        config = load(f, Loader=CSafeLoader)
        config["handlers"]["console"]["level"] = log_level
        dictConfig(config)
    return


def get_log_level(verbose: bool) -> int:
    if verbose:
        return logging.DEBUG
    else:
        return logging.INFO


def write_scd(scd_path: Path, scl_tree):
    scl_tree.write(
        str(scd_path),
        pretty_print=True,
        encoding="UTF-8",
        xml_declaration=True,
    )


@click.command
@click.option(
    "--debug/--no-debug",
    default=False,
    help="Use this flag to enable more verbose logging to the console.",
)
@click.option(
    "-f",
    "--scl-file-path",
    help=("Provide the path to a directory tree containing SCD files."),
    type=click.Path(exists=True, dir_okay=True, file_okay=False),
)
@click.pass_context
# @click.version_option(version=version("iop_f2f"), prog_name="iop")
def cli(ctx, scl_file_path, debug):
    print("*" * 82)
    start_time = datetime.now()
    print(f"Application Started at: {start_time}")
    print("*" * 82)
    ctx.ensure_object(dict)
    log_level = get_log_level(debug)
    configure_logging(log_level)
    logger = logging.getLogger(__name__)
    scl_file_path = Path(scl_file_path)
    for scl_file in scl_file_path.rglob("*.scd"):
        logger.info(f"SCD File Found at {scl_file}")
        scl_tree = etree.parse(str(scl_file), parser=etree.XMLParser(strip_cdata=False))
        ctx.obj["scl-file-path"] = scl_file_path
        ctx.obj["scl-tree"] = scl_tree
        gses = scl_tree.xpath(
            "scl:Communication/scl:SubNetwork/scl:ConnectedAP/scl:GSE",
            namespaces=scl_namespaces,
        )
        for gse in gses:
            if gse.xpath("scl:Address/scl:P[@type='IP']", namespaces=scl_namespaces):
                mac_address_element = next(
                    iter(
                        gse.xpath(
                            "scl:Address/scl:P[@type='MAC-Address']",
                            namespaces=scl_namespaces,
                        )
                    ),
                    None,
                )
                if mac_address_element is not None:
                    mac_address_element.getparent().remove(mac_address_element)
        print(scl_file.resolve())
        write_scd(scl_file.resolve(), scl_tree)


if __name__ == "__main__":
    cli()
